// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "PushNE",
    products: [
        .library(
            name: "PushNE",
            targets: ["PushNE"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "PushNE",
            url: "https://artifactory.i-dgtl.ru/push-libs-ios/push-ne/3.0.4/push-ne-3.0.4.zip",
            checksum: "f1c1b2fc476f56d93e2e7199fbed3d07cd7b891705eef1106b875d0c1fdfca2e"
        ),
    ]
)

